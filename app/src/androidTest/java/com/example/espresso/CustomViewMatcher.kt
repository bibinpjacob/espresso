package com.example.espresso

import android.view.View
import android.widget.EditText
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import java.util.regex.Pattern

val EMAIL_PATTERN = Pattern.compile(
    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"
)

fun passwordSizeMatcher(matcherSize: Int): Matcher<View> {

    return object : BoundedMatcher<View, EditText>(EditText::class.java) {

        override fun describeTo(description: Description) {
            description.appendText("required password size: $matcherSize")
        }

        override fun matchesSafely(item: EditText?): Boolean {
            return matcherSize <= item?.text.toString().length
        }
    }
}

fun phoneNumberValidateMatcher(phone_number: Int): Matcher<View> {

    return object : BoundedMatcher<View, EditText>(EditText::class.java) {
        override fun describeTo(description: Description?) {

        }

        override fun matchesSafely(item: EditText?): Boolean {

            return phone_number == item?.text.toString().length

        }
    }
}

fun emailValidationMatcher(): Matcher<View> {

    return object : BoundedMatcher<View, EditText>(EditText::class.java) {

        override fun describeTo(description: Description?) {

        }

        override fun matchesSafely(item: EditText?): Boolean {
            val result: Boolean

            result = isValidEmail(item?.text.toString())

            return result
        }
    }
}

fun isValidEmail(email: CharSequence?): Boolean {
    return email != null && EMAIL_PATTERN.matcher(email).matches()
}
