package com.example.espresso

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class LoginActivity : AppCompatActivity() {

    private val button_login by lazy {
        findViewById<Button>(R.id.login_button)
    }
    private val button_register by lazy {
        findViewById<Button>(R.id.register_button)
    }

    private val editText_username by lazy {
        findViewById<EditText>(R.id.user_name)
    }

    private val editText_password by lazy {
        findViewById<EditText>(R.id.password)
    }

    private val textView_result by lazy {
        findViewById<TextView>(R.id.login_result)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button_login.setOnClickListener {

            if (editText_username.text.toString().equals("Bibin") && editText_password.text.toString().equals("123456")) {
                loginSuccess()
                val intent = Intent(this,HomeActivity::class.java)
                intent.putExtra("user_name",editText_username.text.toString())
                startActivity(intent)
            }
            else
                loginFailed()
        }

        button_register.setOnClickListener {

            val intent = Intent(this,RegisterUser::class.java)
            startActivity(intent)

        }
    }

    fun loginSuccess() {
        textView_result.text = getString(R.string.login_success)
    }

    fun loginFailed() {
        textView_result.text = getString(R.string.login_failed)
    }

}
