package com.example.espresso

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

class RegisterUser : AppCompatActivity() {

    private val spinner_group by lazy {
        findViewById<Spinner>(R.id.sp_blood_group)
    }
    private val button_register by lazy {
        findViewById<Button>(R.id.btnRegister)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_user)


        // Initializing a String Array
        val colors = arrayOf("A+","A-","B+","B-","O+","O-","AB+","AB-")

        // Initializing an ArrayAdapter
        val adapter = ArrayAdapter(
            this, // Context
            android.R.layout.simple_spinner_item, // Layout
            colors // Array
        )

        // Set the drop down view resource
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)

        // Finally, data bind the spinner object with dapter
        spinner_group.adapter = adapter;

        spinner_group.onItemSelectedListener=object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                Toast.makeText(applicationContext, "Selected : ${parent?.getItemAtPosition(position).toString()}",Toast.LENGTH_SHORT).show()

            }

        }


        button_register.setOnClickListener {

            val intent = Intent(this,LoginActivity::class.java)
            startActivity(intent)

        }

    }
}
