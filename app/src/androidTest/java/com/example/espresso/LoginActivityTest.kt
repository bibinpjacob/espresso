package com.example.espresso

import android.util.Log
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginActivityTest {
    //rule is initialized

    @Rule
    @JvmField
    val rule = getRule()


    private val correct_username = "Bibin"
    private val correct_password = "123456"
    private val wrong_password = "test123"

    private fun getRule(): ActivityTestRule<LoginActivity> {
        Log.e("Initalising rule", "getting LoginActivity")
        return ActivityTestRule(LoginActivity::class.java)
    }

    companion object {

        @BeforeClass
        @JvmStatic
        fun before_class_method() {
            Log.e("@Before Class", "this is run before anything")
        }

        @AfterClass
        @JvmStatic
        fun after_class_method() {
            Log.e("@After Class", "this is run after everything")
        }

    }

    @Before
    fun before_test_method() {
        Log.e("@Before", "this is run before every test function")
    }

    @Test
    fun loginSuccess(){

        Log.e("@Test","Performing login success test")
        onView(withId(R.id.user_name)).perform(typeText(correct_username), closeSoftKeyboard())
        onView(withId(R.id.password)).perform(typeText((correct_password)), closeSoftKeyboard())
        onView(withId(R.id.password)).check(matches(passwordSizeMatcher(6)))
        onView(withId(R.id.login_button)).perform(click())

       // showingSucccess()
    }


    fun showingSucccess(){
        onView(withId(R.id.login_result))
            .check(matches(withText(R.string.login_success)))
    }
}