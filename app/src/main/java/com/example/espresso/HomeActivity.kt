package com.example.espresso

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {


  /*  private val tv_username by lazy {
        findViewById<TextView>(R.id.tv_username)
    }*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val inputData = intent.extras
        val input = inputData!!.getString("user_name")
        tv_username.setText("welcome $input")
    }
}
