package com.example.espresso

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class HomeActivityTest {

    @Rule
    @JvmField
    val rule=getRule()
    private val userName = "User"

    private fun getRule():ActivityTestRule<HomeActivity>{
        // third parameter is set to false which means the activity is not started automatically
        return ActivityTestRule(HomeActivity::class.java,true,false)
    }


    @Test
    fun startActivityWithBundle() {
        val intent = Intent()
        intent.putExtra("user_name", userName)
        rule.launchActivity(intent)

        onView(withId(R.id.tv_username)).check(matches(withText("welcome $userName")))
    }

}
