package com.example.espresso

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.*
import org.junit.runner.RunWith
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.CoreMatchers.*
import org.hamcrest.Matchers.`is`


@RunWith(AndroidJUnit4::class)
class RegisterUserTest {

    @Rule
    @JvmField
    val rule = getRule()

    private val selectionText = "A-"
    private val user_name = "User"
    private val phone = "1234567890"
    private val email = "abc@gmail.com"
    private val age = "22"
    private val place = "Kottayam"

    private fun getRule(): ActivityTestRule<RegisterUser> {

        Log.e("Initalising rule", "getting RegisterUser")
        return ActivityTestRule(RegisterUser::class.java)
    }

    companion object {

        @BeforeClass
        @JvmStatic
        fun before_class_method() {
            Log.e("@Before Class", "this is run before anything")
        }

        @AfterClass
        @JvmStatic
        fun after_class_method() {
            Log.e("@After Class", "this is run after everything")
        }

    }

    @Before
    fun before_test_method() {
        Log.e("@Before", "this is run before every test function")
    }

    @Test
    fun registerUserSuccess() {
        Log.e("@Test", "Performing Register user success test")

        // ViewActions.closeSoftKeyboard()

        onView(withId(R.id.et_name)).perform(ViewActions.typeText(user_name), ViewActions.closeSoftKeyboard())
        // passes if the name  does not match the empty string
        onView(withId(R.id.et_name)).check(matches(not(withText(""))));
        onView(withId(R.id.et_place)).perform(ViewActions.typeText((place)), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.et_phone)).perform(ViewActions.typeText((phone)), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.et_phone)).check(matches(passwordSizeMatcher(10)))
        onView(withId(R.id.et_email)).perform(ViewActions.typeText((email)), ViewActions.closeSoftKeyboard())
        emailValidationTest()
        onView(withId(R.id.et_age)).perform(ViewActions.typeText((age)), ViewActions.closeSoftKeyboard())

        onView(withId(R.id.sp_blood_group)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)), `is`(selectionText))).perform(click())
        onView(withId(R.id.sp_blood_group)).check(matches(withSpinnerText(containsString(selectionText))))
        onView(withId(R.id.btnRegister)).perform(click())

        var loginObj = LoginActivityTest()
        loginObj.loginSuccess()

    }

    @Test
    fun selectSpinnerItemByPositionTest() {
        onView(withId(R.id.sp_blood_group)).perform(click());
        onData(anything()).atPosition(1).perform(click());
        onView(withId(R.id.sp_blood_group)).check(matches(withSpinnerText(containsString(selectionText))));
    }

    @Test
    fun emailValidationTest() {
       // onView(withId(R.id.et_email)).perform(ViewActions.typeText(email), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.et_email)).check(matches(emailValidationMatcher()))
    }
}